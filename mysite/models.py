from django.db import models
from django.contrib.auth.models import User

# class ReportType(models.Model): 
#         reportId = models.IntegerField()
# 	reportName = models.CharField(max_length=128)
#         def __str__(self):
#                 return str(self.reportName)
   
# class ReportOption(models.Model):    
# 	reportOptionID = models.IntegerField()
#         reportOptionName = models.CharField(max_length=128)
#         reportType = models.ForeignKey(ReportType)
#         def __str__(self):
#                 return str(self.reportOptionName)
   
# class ReportOptionField(models.Model):    
# 	fieldName = models.CharField(max_length=128)
#         fieldDescription = models.CharField(max_length=128)
#         sortOrder = models.BooleanField()
#         reportOption = models.ForeignKey(ReportOption)
#         def __str__(self):
#                 return str(self.fieldName)

# class ReportDefaultField(models.Model):
#         reportDefaultFieldNameId = models.IntegerField()
#         reportDefaultFieldName = models.CharField(max_length=128)
#         reportOptionfield = models.ForeignKey(ReportOption)
#         def __str__(self):
#                 return str(self.reportDefaultFieldName)

# class Report(models.Model):    
#         runBy = models.CharField(max_length=128)
#         reportType = models.ForeignKey(ReportType)
#         def __str__(self):
#                 return str(self.reportOptionName)

# Identify a unique REFERENCE
class Reference (models.Model): 
        referenceId = models.IntegerField()
	referenceLabel = models.CharField(max_length=128)
	referenceDescription = models.CharField(max_length=128) 
        categoryDescription = models.ForeignKey(Category)
        def __str__(self):
                return str(self.referenceLabel)

# Identify a unique CATEGORY
class Category (models.Model):    
	categoryId = models.IntegerField()
        categoryDescription = models.CharField(max_length=128)
        def __str__(self):
                return str(self.categoryDescription)

# Identify a unique COLOR 
class Color (models.Model):
	colorId = models.CharField(max_length=128)
	colorGroup = models.CharField(max_length=128)
	colorDescription = models.CharField(max_length=128)
        colorNumber = models.IntegerField(null=True, blank=True)
        sizeRange = models.ForeignKey(SizeRange)
        def __str__(self):
                return str(self.colorDescription)

# Define a unique SIZERANGE
class SizeRange(models.Model): 
	sizeRangeId = models.CharField(max_length=128)  
	sizeRangeGroup = models.CharField(max_length=128)
        sizeRangeDescription = models.CharField(max_length=128)
        sizeRangeCode = models.IntegerField(null=True, blank=True)
        referenceLabel = models.ForeignKey(Reference)
        def __str__(self):
                return str(self.sizeRangeDescription)


# Define a unique STYLE (CATEGORY + REFERENCE + SIZERANGE + COLORWAY)
class Style(models.Model):
        styleId = models.CharField(max_length=128)
        styleNumber = models.IntegerField(null=True, blank=True)
        def __str__(self):
                return str(self.styleNumber)
